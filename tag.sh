#!/usr/bin/env bash

### TAG FOR 3.5.x ###

OLD_TAG=$(git tag --sort=-creatordate | grep -E "3.5.(.*)-release" -m 1)

echo OLDTAG=$OLD_TAG

# Remove `-release`
OLD_TAG=${OLD_TAG%-release}

# Replace . with space so can split into an array
VERSION_BITS=(${OLD_TAG//./ })

# Get number parts and increase last one by 1
VNUM1=${VERSION_BITS[0]}
VNUM2=${VERSION_BITS[1]}
VNUM3=${VERSION_BITS[2]}
VNUM3=$((VNUM3+1))

# Create new tag
NEW_TAG="${VNUM1}.${VNUM2}.${VNUM3}-release"

echo $VNUM3

echo NEW_TAG=$NEW_TAG

BRANCH=`git rev-parse --abbrev-ref HEAD`

echo BRANCH=$BRANCH

git tag -a $NEW_TAG -m "Created from '$BRANCH' by HiepTK"

git push origin $NEW_TAG

# Ref: https://stackoverflow.com/questions/3760086/automatic-tagging-of-releases
